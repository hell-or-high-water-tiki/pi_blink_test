# Located on the Raspberry Pi
# MQTT LED Client
# Continuously blink to to message

import RPi.GPIO as GPIO
import time
import paho.mqtt.client as mqtt
 
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc, flag):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() - if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("Experience")

 
# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, message):
    print("message received " ,str(message.payload.decode("utf-8")))
    print("message topic=",message.topic)
    print("message qos=",message.qos)
    print("message retain flag=",message.retain)

    if "blink" in str(message.payload):
        print("Received Blink message, start blinking")
        blinks = 20
        LED_PIN = 17

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(LED_PIN, GPIO.OUT)
        for i in range(blinks):
            time.sleep(1)
            GPIO.output(LED_PIN, GPIO.HIGH)
            time.sleep(1)
            GPIO.output(LED_PIN, GPIO.LOW)

        GPIO.cleanup()
    else:
        print ("Payload not recieved")

def on_log(client, userdata, level, buf):
    print("log: ",buf)
 
# Create an MQTT client and attach our routines to it.
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.on_log=on_log
 
client.connect("hhw-master.broker", port=1883, keepalive=60)
 
# Process network traffic and dispatch callbacks. This will also handle
# reconnecting. Check the documentation at
# https://github.com/eclipse/paho.mqtt.python
# for information on how to use other loop*() functions
client.loop_forever()