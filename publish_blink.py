# MQTT LED Project
# Publish a message to hhw-pi1 client for blinking led

# import paho.mqtt.publish as publish

# publish(topic="topic/experience", payload="blink", hostname="hhw-pi1.client")
# print("Trigger Experience to Pi")

import paho.mqtt.client as mqtt
import time

mqttBroker = "localhost"
client = mqtt.Client("Experience Manager")
client.connect(mqttBroker)

while True:
    client = mqtt.Client("Experience Manager")
    client.connect(mqttBroker)
    client.publish("Experience", "blink")
    print("Just published published 'blink' to Topic Experience")
    time.sleep(150)